#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
#include <set>
#include<map>
using std::set;
using std::multiset;
#include <strings.h>
#include <vector>
using std::vector;
using namespace std;

void rv();
void ic();
void uniq();
void none();
void RF();
void UF();
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* NOTE: set<string,igncaseComp> S; would declare a set S which
 * does its sorting in a case-insensitive way! */
map<string,int> a;
map<string,int,igncaseComp>b;

int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	/* TODO: write me... */
	string line;
	while (getline(cin,line))
	{
		b[line]++;
		a[line]++;
	}
	if (!descending&& !ignorecase&& !unique)none();
	else if (descending&& ignorecase) RF();
	else if (unique&& ignorecase)UF();
	else if( descending)rv();
	else if (unique)uniq();
	else if (ignorecase) ic();

	return 0;
}
void rv()
{
	map<string, int>::reverse_iterator i;
	for (i= a.rbegin(); i !=a.rend(); ++i)
	{
		for (int j=0; j<(*i).second; j++)
		cout<<(*i).first<<"\n";
	}
}
void ic()
{
	map<string,int>::iterator i;
	for (i= b.begin(); i != b.end(); i++)
	{
		cout <<(*i).first<<"\n";
	}
}
void uniq()
{
	map<string,int>::iterator i;
	for(i =a.begin();i !=a.end(); i++)
	{
		cout <<(*i).first<<"\n";
	}
}
void none()
{
	map <string,int>::iterator i;
	for( i= a.begin(); i !=a.end(); i++)
	{
		for(int j =0; j<(*i).second; j++)
		cout <<(*i).first<<"\n";
	}
}
void RF()
{
	map<string, int, igncaseComp>:: reverse_iterator i;
	for (i=b.rbegin(); i !=b.rend();i++)
	{
		cout<< (*i).first<<"\n";
	}
}
void UF()
{
	map<string, int, igncaseComp>::iterator i;
	for(i=b.begin(); i !=b.end(); i++)
	{
		cout<<(*i).first<<"\n";
	}
}