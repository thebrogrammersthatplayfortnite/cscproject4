/*
Amrit Singh
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:8hours
 */
#include <iostream>
#include <string>
using std::string;
#include <set>
using std::set;
using std::cin;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <unordered_map>
#include <sstream>
using namespace std;
using std::cout;
using std::endl;


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

string y;
string arg;
string wrd;
int wc;
int i;
string line;
int line_count;
int num_of_bytes;
int word_count;
int maxlength=0;
int numb_of_char=0;
std::unordered_map<std::string, unsigned> dict;

int maxline(string y){
  if(y.length() > maxlength){
  maxlength = y.length();
  }
}

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};

		getline(cin, line);
		maxline(line);
    stringstream ss(line);
    while (getline(ss,wrd,' ')){
      numb_of_char = wrd.length()+numb_of_char;
      wc++;
      ++dict[wrd];
      }
    num_of_bytes=num_of_bytes+line.size();
    word_count=word_count+wc;
    line_count++;


	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
			cout<<num_of_bytes<<endl;
				break;
			case 'l':
				linesonly = 1;
			cout<<line_count<<endl;
				break;
			case 'w':
				wordsonly = 1;
			cout<<wc<<'\t'<<endl;
				break;
			case 'u':
				uwordsonly = 1;
			cout<<dict.size()<<endl;
				break;
			case 'L':
				longonly = 1;
			cout<<maxlength<<endl;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	return 0;
}